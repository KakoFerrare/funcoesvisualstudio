﻿using FirebirdSql.Data.FirebirdClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Funcoes.DAO
{
    public class Pesquisa
    {
        private StringBuilder strQuery = new StringBuilder();
        private FbConnection conn = new FbConnection();


        public DataTable Todos(string table, string connection, string[] arrayCollumNames)
        {
            string collum = string.Join(",", arrayCollumNames);

            strQuery = new StringBuilder();
            DataTable ListaItem = new DataTable();
            strQuery.Append(" Select " + collum + " from " + table);
            using (conn = new FbConnection(connection))
            {
                conn.Open();
                try
                {
                    using (FbCommand cmd = conn.CreateCommand())
                    {
                        cmd.CommandType = CommandType.Text;
                        cmd.CommandText = strQuery.ToString();
                        using (DbDataReader reader = cmd.ExecuteReader())
                        {
                            ListaItem.Load(reader);
                        }
                    }
                }
                catch (Exception e)
                {
                    ListaItem = new DataTable();
                    MessageBox.Show("Erro ao carregar: " + e.Message, "Erro em execução", MessageBoxButtons.OK);
                }
            }
            return ListaItem;
        }

        internal DataTable Buscar(string table, string[] arrayCollumNames, string campo, string criterio, string busca, string connection)
        {
            if (criterio.Equals("inicio") || criterio.Equals("fim") || criterio.Equals("LIKE"))
            {

                busca = (criterio.Equals("LIKE") ? "%" + busca + "%" : (criterio.Equals("inicio") ? busca + "%" : "%" + busca));
                criterio = "LIKE";
            }
            string collum = string.Join(",", arrayCollumNames);

            strQuery = new StringBuilder();
            DataTable ListaItem = new DataTable();
            strQuery.Append(" Select " + collum + " from " + table + " WHERE UPPER(CAST(" + campo + " AS VarChar(100)))" + criterio + "'" + busca.ToUpper() + "'");
            using (conn = new FbConnection(connection))
            {
                conn.Open();
                try
                {
                    using (FbCommand cmd = conn.CreateCommand())
                    {
                        cmd.CommandType = CommandType.Text;
                        cmd.CommandText = strQuery.ToString();
                        cmd.Parameters.Add("@Busca", busca);
                        using (DbDataReader reader = cmd.ExecuteReader())
                        {
                            ListaItem.Load(reader);
                        }
                    }
                }
                catch (Exception e)
                {
                    ListaItem = new DataTable();
                    MessageBox.Show("Erro ao carregar: " + e.Message, "Erro em execução", MessageBoxButtons.OK);
                }
            }
            return ListaItem;
        }

    }
}

﻿using FirebirdSql.Data.FirebirdClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Funcoes.DAO
{
    //Commit
    public class Login
    {
        FbConnection conn = new FbConnection();
        private StringBuilder strQuery = new StringBuilder();
        public string[] LogIn(string nome, string senha, string stringConnection)
        {
            String[] user = new String[6];
            strQuery = new StringBuilder();
            strQuery.Append(" SELECT idusuario, nome, nivel, ativo, idpac ")
            .Append(" FROM usuarios ")
            .Append(" WHERE UPPER(login) = @Nome AND senha = @Senha");

            using (conn = new FbConnection(stringConnection))
            {
                conn.Open();
                try
                {
                    using (FbCommand cmd = conn.CreateCommand())
                    {
                        cmd.CommandType = CommandType.Text;
                        cmd.CommandText = strQuery.ToString();
                        cmd.Parameters.Add("@Nome", nome);
                        cmd.Parameters.Add("@Senha", senha);
                        using (DbDataReader reader = cmd.ExecuteReader())
                        {
                            if (reader.Read())
                            {
                                user[0] = (reader["idusuario"] == DBNull.Value ? null : Convert.ToString(reader["idusuario"]));
                                user[1] = (reader["nome"] == DBNull.Value ? null : Convert.ToString(reader["nome"]));
                                user[2] = (reader["nivel"] == DBNull.Value ? null : Convert.ToString(reader["nivel"]));
                                user[3] = (reader["ativo"] == DBNull.Value ? null : Convert.ToString(reader["ativo"]));
                                user[4] = (reader["idpac"] == DBNull.Value ? null : Convert.ToString(reader["idpac"]));
                            }
                            else
                            {
                                user[5] = "Usuário não encontrado, verifique suas credenciais.";
                            }
                            if(user[3] == "0")
                            {
                                user[5] = "Usuário encontrado mas não está em atividade, contate o administrador.";
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    user[5] = "Erro ao efetuar o login." + e.Message;
                }
                conn.Close();
            }
            return user;

        }
    }
}

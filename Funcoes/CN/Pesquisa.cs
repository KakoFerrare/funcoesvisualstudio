﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Funcoes.CN
{
    public class Pesquisa
    {
        public string[] buscaPor = { };
        public string[] criterio = { "Palavra", "Começando com", "Frase Exata", "Terminando com" };
        public string[] criterioStatment = { "LIKE", "inicio", "=", "fim" };
        public string table;
        public string[] headerCollumName;
        public string[] tableCollumName;

        public void ContaCorrente()
        {
            this.table = "CONTACORRENTE";
            this.buscaPor = new string[] { "Conta Corrente", "CPF", "Nome" };
            headerCollumName = new string[] { "Conta", "Documento", "Nome" };
            tableCollumName = new string[] { "IDCONTA", "IDCPF1" ,"NOME1_CHEQUE" };
        }

        public void NossoNumero()
        {
            this.table = "COB_OCORR";
            this.buscaPor = new string[] { "Nosso Número", "Conta" };
            headerCollumName = new string[] { "Nosso Número", "Conta", "Prefixo" };
            tableCollumName = new string[] { "NOSSONR", "IDCONTA", "PREFIXO" };
        }


        public DataTable Todos(string table, string stringConnection)
        {
            DataTable lista = new DataTable();
            lista = new DAO.Pesquisa().Todos(this.table, stringConnection, tableCollumName);
            return lista;
        }

        public DataTable Buscar(string table, int indexEncontrar, int indexCriterio, string busca ,string stringConnection)
        {
            DataTable lista = new DataTable();
            lista = new DAO.Pesquisa().Buscar(this.table, tableCollumName, this.tableCollumName[indexEncontrar], this.criterioStatment[indexCriterio], busca, stringConnection);
            return lista;
        }
    }
}

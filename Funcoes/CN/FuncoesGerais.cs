﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Funcoes.CN
{
    public class FuncoesGerais
    {
        public string FormatConta(string conta)
        {
            if (!Object.ReferenceEquals(conta, null))
            {
                if (conta.Length <= 10)
                {
                    conta = FormatZerosEsquerda(conta, 10);
                    conta = conta.Insert(9, "-");
                }
            }
            return conta;
        }

        public string FormatZerosEsquerda(string texto, int index)
        {
            if (!Object.ReferenceEquals(texto, null))
            {
                texto = texto.PadLeft(index, '0');
            }
            return texto;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Funcoes.CN
{
    public class Login
    {
        private DAO.Login DAOLogin = new DAO.Login();

        public string[] LogIn(string nome, string senha,  string stringConnection)
        {
            senha = Encript(senha);
            return DAOLogin.LogIn(nome, senha, stringConnection);
        }


        private string Encript(string senha)
        {
            string crypt = "";
            int tamanho = senha.Length;
            long caracter = 0;
            for (int i = 1; i <= tamanho; i++)
            {
                try
                {
                    caracter = Convert.ToChar(senha.Substring(i-1, 1).ToUpper()) ^ i;
                }
                catch(Exception)
                {
                    
                }
                crypt = crypt + caracter.ToString("000");
            }
            return crypt;
        }
    }
}

